In main:
* Create board (requires number of players as first user input)
* Board has variable of "number of players"
* Board has a dictionary with 
    * "set of Columns", each Column has a "length" and "pip_value"
    * Need some sort of Player progress tracker

To do: 
* Make a Player class
* Determine board data structure

Pseudo-code:  
Create die class  
Create Roll Dice method  
Create options matrix  
Create pick interface  
Create board structure  
Update board  

Player class:  
Needs column trackers  
-1 for someone else owns the column    